function reverseNumber(num) {
    if(num === parseInt(num, 10)){
        let digit, result = 0;
        while(num) {
            digit = num % 10;
            result = result*10 + digit;
            num = num/10 | 0;
        }
        return result;
    }
    return null;
}

function forEach(arr, func) {
    for(let i = 0; i < arr.length;i++){
        arr[i] = func(arr[i])
    }
}

function map(arr, func) {
    forEach(arr, func)
    return arr;
}

function filter(arr, func) {
    let initialArr = [...arr];
    let filterA = [];
    forEach(arr, func)
    for(let i = 0;i<arr.length; i++) {
        if(arr[i]) {
            filterA.push(initialArr[i]);
        }
    }
    return filterA;
}

function getAdultAppleLovers(data) {
    let filteredA = filter(data, function(el) {
        return el.age>18 && el.favoriteFruit === 'apple';
    });
    let res = map(filteredA, (el) => el.name);
    return res;
}

function getKeys(obj) {
    let res = [];
    for(let i in obj) {
        res.push(i);
    }
    return res;
}

function getValues(obj) {
    let res = [];
    for(let i in obj) {
        res.push(obj[i]);
    }
    return res;
}

function showFormattedDate(dateObj) {
    let res = 
    `It is ${dateObj.getDate()} of ${dateObj.toLocaleString('en', {month: 'short'})}, ${dateObj.getFullYear()}`;
    return res;
}
