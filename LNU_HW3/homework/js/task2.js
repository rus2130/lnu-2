let word = prompt('Enter the word:');
if(word.trim().length === 0) {
    alert('Invalid value');
}else{
    let oddEven = word.length % 2 === 0;
    let firstChar = word.charAt(word.length / 2 - 1);
    let secondChar = word.charAt(word.length / 2);
    oddEven ? alert(firstChar.concat(secondChar)) : alert(secondChar);
}