(function ask() {
    let initialAmount = Number(prompt('Enter check number:'));
    let percentage = Number(prompt('Enter tip percentage:'));
    let totalSum = initialAmount + initialAmount / 100 * percentage;

    if(isNaN(percentage)
        || isNaN(initialAmount)
        || percentage > 100
        || percentage < 0
        || initialAmount < 0) {
            alert('Invalid input data');
            ask();
    }else {
        alert(`
        Check number: ${initialAmount}
        Tip: ${percentage}%
        Tip amount: ${Number.isInteger(totalSum - initialAmount) ? totalSum - initialAmount 
            : (totalSum - initialAmount).toFixed(2)}
        Total sum to pay: ${Number.isInteger(totalSum) ? totalSum : totalSum.toFixed(2)}    
            `);
    }
})();